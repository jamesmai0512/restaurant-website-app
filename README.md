## Table of contents
* [General app information](#general-app-information)
* [Technologies](#technologies)
* [Setup](#setup)

## general-app-information
[Wiki](https://gitlab.com/jamesmai0512/restaurant-website-app/-/wikis/Restaurant-app-wiki)
	
## Technologies
Tech stack:
- HTML/CSS
- TypeScript
- React

Test libraries I used
- React testing library
- Jest

Project is created with
- pnpm and vite
- Template with React is init with TypeScript

	
## Setup

**Clone project**
```
git clone https://gitlab.com/jamesmai0512/restaurant-website-app.git
```

**Require package:**
- node version 17.7.1, install node with command:
`nvm install 17.7.1`  `nvm use 17`
- pnpm version 6.32.3, install pnpm with command:
`npm install -g pnpm@6.32.3`

**To run this project**
Install it locally using pnpm:

```
$ cd /restaurant-website
$ pnpm install
$ pnpm dev
```

To run the test of the Project:
```
$ pnpm test
```

**Deploy project to git**
```
git add. //to add all file
``` 

```
git commit -m 'feat: Your message'
```
```
git push origin your-branch
```

**src folder structure form of a tree**

├── src
│   ├── app.tsx
│   ├── assets
│   │   ├── icons
│   │   │   └── S.svg
│   │   └── img
│   │       └── restaurant 1.png
│   ├── commonStyledComponents
│   │   ├── app.ts
│   │   ├── cart.ts
│   │   └── menu.ts
│   ├── components
│   │   ├── cart
│   │   │   ├── __test__
│   │   │   │   └── cart.test.tsx
│   │   │   └── index.tsx
│   │   ├── confirmOrderBtn
│   │   │   └── index.tsx
│   │   ├── food
│   │   │   └── index.tsx
│   │   ├── foodCart
│   │   │   └── index.tsx
│   │   ├── foodDetailModal
│   │   │   ├── index.tsx
│   │   │   └── style.ts
│   │   ├── footer
│   │   │   └── index.tsx
│   │   ├── home
│   │   │   ├── index.tsx
│   │   │   └── style.ts
│   │   ├── menu
│   │   │   ├── __test__
│   │   │   │   └── menu.test.tsx
│   │   │   └── index.tsx
│   │   ├── sideBar
│   │   │   ├── index.tsx
│   │   │   └── style.ts
│   │   └── thankYouModal
│   │       ├── index.tsx
│   │       └── style.ts
│   ├── main.tsx
│   ├── styles
│   │   └── reset.css
│   ├── types
│   │   ├── Cart.ts
│   │   └── SushiFood.ts