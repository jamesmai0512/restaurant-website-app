import { useState } from "react";
import { Route, Routes } from "react-router-dom";
import foods from "../db/foods.json";
import Cart from "./components/Cart";
import { ContainerStyles } from "./components/Common/CommonStyledComponents/layout";
import { FoodsProvider } from "./components/Common/DataProvider/FoodsContext";
import { useSelectedQuantity } from "./components/Common/DataProvider/QuantityContext";
import Footer from "./components/Common/Footer";
import SideBar from "./components/Common/SideBar";
import Home from "./components/Home";
import Menu from "./components/Menu";

function App() {
  const { selectedFoodQuantity, setSelectedFoodQuantity } =
    useSelectedQuantity();
  return (
    <>
      {
        <FoodsProvider>
          <Routes>
            <Route
              path="/menu"
              element={
                <>
                  <ContainerStyles>
                    <SideBar selectedFoodQuantity={selectedFoodQuantity} />
                    <Menu
                      foods={foods}
                      setSelectedFoodQuantity={setSelectedFoodQuantity}
                      selectedFoodQuantity={selectedFoodQuantity}
                    />
                    <Footer />
                  </ContainerStyles>
                </>
              }
            />
            <Route
              path="/cart"
              element={
                <>
                  <ContainerStyles>
                    <SideBar selectedFoodQuantity={selectedFoodQuantity} />
                    <Cart setSelectedFoodQuantity={setSelectedFoodQuantity} />
                    <Footer />
                  </ContainerStyles>
                </>
              }
            />
            <Route path="/" element={<Home />} />
          </Routes>
        </FoodsProvider>
      }
    </>
  );
}

export default App;
