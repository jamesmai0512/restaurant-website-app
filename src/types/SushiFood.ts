export type SushiFood = {
  id: string;
  imageUrl: string;
  name: string;
  quantity: number;
  price: number;
};
