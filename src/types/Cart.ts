import { SushiFood } from "./SushiFood";

export interface ICart {
  food: SushiFood;
  quantity: number;
  id: string;
}
