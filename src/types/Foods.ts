import { ICart } from "./Cart";

export interface Foods {
  foods: ICart[];
  setFoods: React.Dispatch<React.SetStateAction<ICart[]>>;
}
