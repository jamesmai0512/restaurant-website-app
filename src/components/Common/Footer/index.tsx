import styled from "styled-components";

const CopyRightText = styled.footer`
  padding-top: 1rem;
  height: 40px;
  font-size: 1rem;
`;

function Footer() {
  return <CopyRightText>Copyright © 2021 Sushi Restaurant</CopyRightText>;
}

export default Footer;
