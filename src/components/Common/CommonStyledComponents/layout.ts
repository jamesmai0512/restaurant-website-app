import styled from "styled-components";

const ContainerStyles = styled.article`
  margin: auto;
  @media (min-width: 112rem) {
    padding: 2rem 15rem 0 20rem;
  }
  @media (max-width: 112rem) {
    padding: 2rem 13rem 0 20rem;
  }
  @media (max-width: 98rem) {
    padding: 2rem 12rem 0 17rem;
  }
  @media (max-width: 90rem) {
    padding: 2rem 8rem 0 15rem;
  }
  display: block;
`;

const ItemContentStyles = styled.section`
  display: flex;
  flex-direction: column;
  flex: 10;
  min-height: 49rem;
`;

export { ContainerStyles, ItemContentStyles };
