import { useState } from "react";
import { ICart } from "../../../types/Cart";
import {
  ConfirmButtonStyles,
  ConfirmOrderStyles,
  SubTotalStyles,
  TotalDetailStyles,
} from "../../Cart/styles";
import ThankYouModal from "../ThankYouModal";

interface Props {
  foods: ICart[];
  subTotal: number;
  setFoods: React.Dispatch<React.SetStateAction<ICart[]>>;
}

function ConfirmOrderBtn({ foods, subTotal, setFoods }: Props) {
  const [isThankYouDisplay, setIsThankYouDisplay] = useState<boolean>(false);
  const handleConfirmOrder = () => {
    setIsThankYouDisplay(true);
    setFoods([]);
  };
  return (
    <>
      <ConfirmOrderStyles>
        <SubTotalStyles>Your Subtotal</SubTotalStyles>

        <TotalDetailStyles>
          <span>Subtotal</span>
          <span>$ {subTotal}</span>
        </TotalDetailStyles>

        <ConfirmButtonStyles>
          {foods.length > 0 && (
            <button
              onClick={() => {
                handleConfirmOrder();
              }}
              name=""
              type="button"
            >
              Confirm Order
            </button>
          )}
        </ConfirmButtonStyles>
      </ConfirmOrderStyles>
      {isThankYouDisplay && (
        <ThankYouModal setIsThankYouDisplay={setIsThankYouDisplay} />
      )}
    </>
  );
}

export default ConfirmOrderBtn;
