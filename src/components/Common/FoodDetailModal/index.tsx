import { ICart } from "../../../types/Cart";
import { SushiFood } from "../../../types/SushiFood";
import { useFoods } from "../DataProvider/FoodsContext";
import {
  AddToCartButtonStyles,
  CancelButtonStyles,
  CloseButtonStyles,
  FoodModalActionStyles,
  FoodModalStyles,
  ModalBackGroundStyles,
} from "./style";

interface Props {
  handleCloseFoodModal: () => void;
  selectedFood: SushiFood;
  setSelectedFoodQuantity: React.Dispatch<React.SetStateAction<number>>;
  selectedFoodQuantity: number;
}

function FoodDetailModal({
  handleCloseFoodModal,
  selectedFood,
  setSelectedFoodQuantity,
  selectedFoodQuantity,
}: Props) {
  const { foods, setFoods } = useFoods();
  const selectedFoodsInCart: ICart[] = foods;
  const handleFoodAddToCart = () => {
    setSelectedFoodQuantity(selectedFoodQuantity + 1);

    const foodCartIndex = selectedFoodsInCart.findIndex(
      (cart) => cart.food.id === selectedFood.id
    );

    let id: string = (Math.random() + 1).toString(36);

    if (foodCartIndex !== -1) {
      selectedFoodsInCart[foodCartIndex].quantity += 1;
    } else {
      selectedFoodsInCart.push({
        food: selectedFood,
        quantity: 1,
        id,
      });
    }

    const newFoods = new Array(...selectedFoodsInCart);
    setFoods(newFoods);
  };
  return (
    <ModalBackGroundStyles>
      <FoodModalStyles>
        <CloseButtonStyles>
          <button onClick={handleCloseFoodModal}>&times;</button>
        </CloseButtonStyles>
        <h2>Food: {selectedFood.name}</h2>
        <img src={selectedFood.imageUrl} />
        <FoodModalActionStyles>
          <AddToCartButtonStyles onClick={handleFoodAddToCart}>
            Add to cart
          </AddToCartButtonStyles>
          <CancelButtonStyles onClick={handleCloseFoodModal}>
            Cancel
          </CancelButtonStyles>
        </FoodModalActionStyles>
      </FoodModalStyles>
    </ModalBackGroundStyles>
  );
}

export default FoodDetailModal;
