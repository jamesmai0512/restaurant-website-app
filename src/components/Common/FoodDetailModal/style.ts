import styled from "styled-components";

const ModalBackGroundStyles = styled.div`
  position: fixed;
  z-index: 1; /* Sit on top */
  padding-top: 5rem;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.4);
  font-family: "Mulish", sans-serif;
`;

const FoodModalStyles = styled.div`
  margin: auto;
  width: 25rem;
  background: white;
  border: 0 solid #000;
  border-radius: 6px;
  padding: 1.25rem;
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  h2 {
    padding: 1rem 0;
  }
`;

const FoodModalActionStyles = styled.div`
  button {
    margin: 5px;
  }
`;

const CloseButtonStyles = styled.div`
  text-align: end;
  button {
    cursor: pointer;
    font-size: 2rem;
    border: none;
    background: none;
  }
`;

const AddToCartButtonStyles = styled.button`
  color: white;
  padding: 0.7rem 1.9rem;
  background: #e6a247;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background: #2fbf30;
    transition: all 0.5s cubic-bezier(0.25, 0.8, 0.25, 1);
  }
`;

const CancelButtonStyles = styled.button`
  color: white;
  padding: 0.7rem 1.4rem;
  color: #ef0000;
  border: 1px solid #ef0000;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    transition: all 0.5s cubic-bezier(0.25, 0.8, 0.25, 1);
    color: white;
    background: #ef0000;
    border-color: white;
  }
`;

export {
  ModalBackGroundStyles,
  FoodModalStyles,
  FoodModalActionStyles,
  CloseButtonStyles,
  AddToCartButtonStyles,
  CancelButtonStyles,
};
