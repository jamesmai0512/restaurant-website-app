import {
  CloseButtonStyles,
  FoodModalActionStyles,
  ModalBackGroundStyles,
} from "../FoodDetailModal/style";
import { CloseModalButtonStyles, ConfirmOrderModalStyles } from "./style";

interface Props {
  setIsThankYouDisplay: React.Dispatch<React.SetStateAction<boolean>>;
}

function ThankYouModal({ setIsThankYouDisplay }: Props) {
  return (
    <ModalBackGroundStyles>
      <ConfirmOrderModalStyles>
        <CloseButtonStyles>
          <span
            onClick={() => {
              setIsThankYouDisplay(false);
            }}
          >
            &times;
          </span>
        </CloseButtonStyles>
        <h1>
          You dishes has been ordered. <br />
          Thank You
        </h1>
        <FoodModalActionStyles>
          <CloseModalButtonStyles
            onClick={() => {
              setIsThankYouDisplay(false);
            }}
          >
            Close
          </CloseModalButtonStyles>
        </FoodModalActionStyles>
      </ConfirmOrderModalStyles>
    </ModalBackGroundStyles>
  );
}

export default ThankYouModal;
