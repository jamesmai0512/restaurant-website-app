import styled from "styled-components";

const ConfirmOrderModalStyles = styled.div`
  margin: auto;
  width: 25rem;
  background: white;
  border: 0 solid #000;
  border-radius: 6px;
  padding: 1.25rem;
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const CloseModalButtonStyles = styled.div`
  color: white;
  padding: 0.7rem 1.4rem;
  color: #ef0000;
  border: 1px solid #ef0000;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    transition: all 0.5s cubic-bezier(0.25, 0.8, 0.25, 1);
    color: white;
    background: #ef0000;
    border-color: white;
  }
`;
export { ConfirmOrderModalStyles, CloseModalButtonStyles };
