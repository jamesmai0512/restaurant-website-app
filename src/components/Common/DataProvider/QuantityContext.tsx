import React, { createContext, useContext, useEffect, useState } from "react";

interface Props {
  children: React.ReactNode;
}

interface FoodQuantity {
  selectedFoodQuantity: number;
  setSelectedFoodQuantity: React.Dispatch<React.SetStateAction<number>>;
}

const FoodQuantityContext = createContext<FoodQuantity>(
  0 as unknown as FoodQuantity
);

function SelectedQuatityProvider({ children }: Props) {
  const [selectedFoodQuantity, setSelectedFoodQuantity] = useState<number>(
    Number(localStorage.getItem("selectedFoodQuantity")) || 0
  );

  useEffect(() => {
    localStorage.setItem(
      "selectedFoodQuantity",
      JSON.stringify(selectedFoodQuantity)
    );
  }, [selectedFoodQuantity]);

  return (
    <FoodQuantityContext.Provider
      value={{ selectedFoodQuantity, setSelectedFoodQuantity }}
    >
      {children}
    </FoodQuantityContext.Provider>
  );
}

const useSelectedQuantity = () => useContext(FoodQuantityContext);

export { useSelectedQuantity, SelectedQuatityProvider };
