import React, { createContext, useContext, useEffect, useState } from "react";
import { ICart } from "../../../types/Cart";
import { Foods } from "../../../types/Foods";

interface Props {
  children: React.ReactNode;
}

const FoodContext = createContext<Foods>([] as unknown as Foods);

function FoodsProvider({ children }: Props) {
  const [foods, setFoods] = useState<ICart[]>(
    JSON.parse(localStorage.getItem("foods") || "[]")
  );

  useEffect(() => {
    localStorage.setItem("foods", JSON.stringify(foods));
  }, [foods]);

  return (
    <FoodContext.Provider value={{ foods, setFoods }}>
      {children}
    </FoodContext.Provider>
  );
}

const useFoods = () => useContext(FoodContext);

export { useFoods, FoodsProvider };
