import CloseIcon from "@mui/icons-material/Close";
import { ICart } from "../../../types/Cart";
import {
  CartDetailStyles,
  CartIconStyles,
  CartItemStyles,
  CartItemWrapperStyles,
  CartNameStyles,
  CartQuantityStyles,
  RemoveDishStyles,
} from "../../Cart/styles";

interface Props {
  foods: ICart[];
  handleCartQuantity: (
    e: React.ChangeEvent<HTMLInputElement>,
    id: string
  ) => void;
  handleRemoveFood: (id: string) => void;
}

function FoodCart({ foods, handleCartQuantity, handleRemoveFood }: Props) {
  return (
    <CartItemWrapperStyles>
      {foods.map((food) => (
        <CartItemStyles key={food.food.id}>
          <CartIconStyles src={food.food.imageUrl} alt={food.food.name} />
          <CartDetailStyles>
            <CartNameStyles>{food.food.name}</CartNameStyles>
            <p>$ {food.food.price}</p>
          </CartDetailStyles>
          <CartQuantityStyles>
            <input
              onChange={(e) => {
                handleCartQuantity(e, food.id);
              }}
              data-testid={food.id}
              type="number"
              defaultValue={food.quantity}
              min="1"
              max="50"
            />
          </CartQuantityStyles>
          <RemoveDishStyles>
            <button
              data-testid={food.food.name}
              type="button"
              name={food.food.name}
              onClick={() => {
                handleRemoveFood(food.id);
              }}
            >
              <CloseIcon />
            </button>
          </RemoveDishStyles>
        </CartItemStyles>
      ))}
    </CartItemWrapperStyles>
  );
}

export default FoodCart;
