interface Props {
  imageUrl: string;
  name: string;
}

export const Food = ({ imageUrl, name }: Props) => {
  return (
    <>
      <img src={imageUrl} alt={name} loading="lazy" />
      <h3>{name}</h3>
    </>
  );
};

export default Food;
