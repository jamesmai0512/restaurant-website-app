import styled from "styled-components";

const SideBarStyles = styled.aside`
  position: fixed;
  top: 2rem;
  left: 10rem;
  @media (max-width: 98rem) {
    left: 5rem;
  }
`;

const SideBarContainerStyles = styled.nav`
  width: 8.375rem;
  background: #0c0b0b;
  border-radius: 24px;
  display: flex;
  flex: 2;
  flex-direction: column;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  a {
    height: 7.4rem;
    display: flex;
    justify-content: center;
    align-items: center;
    text-decoration: none;
    &.active {
      background: #f1d5bb;

      svg,
      span {
        color: black;
      }
    }
    svg {
      color: white;
      width: 2.5rem;
      height: 2.5rem;
    }
  }
`;

const FoodQuantityStyles = styled.span`
  color: #f1d5bb;
  font-size: 1.875rem;
  position: relative;
`;

export { SideBarStyles, SideBarContainerStyles, FoodQuantityStyles };
