import {
  Home,
  LocalDining,
  LocationOnOutlined,
  Logout,
  ShoppingCartOutlined,
} from "@mui/icons-material";
import { NavLink } from "react-router-dom";
import S from "../../../assets/icons/S.svg";
import {
  FoodQuantityStyles,
  SideBarContainerStyles,
  SideBarStyles,
} from "./style";
interface Props {
  selectedFoodQuantity: number;
}
function SideBar({ selectedFoodQuantity }: Props) {
  return (
    <SideBarStyles>
      <SideBarContainerStyles>
        <NavLink to={"/"}>
          <img src={S} alt="" />
        </NavLink>
        <NavLink to={"/"}>
          <Home />
        </NavLink>
        <NavLink to={"/menu"}>
          <LocalDining />
        </NavLink>
        <NavLink to={"/cart"}>
          <ShoppingCartOutlined />
          {selectedFoodQuantity > 0 && (
            <FoodQuantityStyles>{selectedFoodQuantity}</FoodQuantityStyles>
          )}
        </NavLink>
        <NavLink to={"/"}>
          <LocationOnOutlined />
        </NavLink>
        <NavLink to={"/"}>
          <Logout />
        </NavLink>
      </SideBarContainerStyles>
    </SideBarStyles>
  );
}

export default SideBar;
