import styled from "styled-components";

interface IImgProps {
  img: string;
}

const HomeStyles = styled.article`
  background-image: url("https://raw.githubusercontent.com/jamesmai0512/sushi-image/main/img/restaurant%201.png");
  color: white;
  padding: 0 34rem;
  @media (max-width: 110rem) {
    padding: 0 22rem;
  }
  @media (max-width: 90rem) {
    padding: 0 15rem;
  }
  @media (max-width: 80rem) {
    padding: 0 11rem;
  }
  padding: 0 34rem;
  font-family: "Mulish", sans-serif;
  background-size: cover;
  height: 100vh;
  display: flex;
  justify-content: center;
  gap: 5rem;
  flex-direction: column;
  align-items: center;
  margin: auto;
`;

const WelcomeLineStyles = styled.h1`
  font-size: 3.75rem;
`;

const CaptionStyles = styled.p`
  font-size: 1.7rem;
  text-align: center;
`;

const ButtonStyles = styled.div`
  display: flex;
  gap: 2rem;
  a {
    text-decoration: none;
    padding: 1.3rem 4.6rem;
    font-weight: 100;
    color: black;
    background: white;
    transition: all 0.3s;
    cursor: pointer;
    &:hover {
      background: #dad9d9;
    }
  }
`;

export { HomeStyles, WelcomeLineStyles, CaptionStyles, ButtonStyles };
