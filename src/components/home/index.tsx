import { useNavigate } from "react-router-dom";

import "./style";
import {
  ButtonStyles,
  CaptionStyles,
  HomeStyles,
  WelcomeLineStyles,
} from "./style";

function Home() {
  return (
    <HomeStyles>
      <WelcomeLineStyles>
        Welcome to <b>Sushi</b> Restaurant
      </WelcomeLineStyles>
      <CaptionStyles>
        People eat with their eyes and Sushi creates an easy way for customers
        to order when they can see beautiful photos of your food
      </CaptionStyles>
      <ButtonStyles>
        <a href="/">ABOUT</a>
        <a href="/menu">MENU</a>
      </ButtonStyles>
    </HomeStyles>
  );
}

export default Home;
