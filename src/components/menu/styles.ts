import styled from "styled-components";

const HeadingStyles = styled.h2`
  font-size: 3.5rem;
  text-decoration: underline;
  text-decoration-color: #f1d5bb;
  text-underline-offset: 0.625rem;
  font-family: "Mulish", sans-serif;
  font-weight: 400;
  margin: 0 1.125rem 1.125rem 0;
`;

const GridContainerStyles = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  @media (max-width: 83rem) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media (max-width: 65rem) {
    grid-template-columns: repeat(2, 1fr);
  }
  gap: 2.5rem;
`;
const LoadMoreStyles = styled.button`
  margin: 1.5rem auto 0;
  background-color: black;
  color: white;
  padding: 1rem 2.5rem;
  margin-top: 1.5rem;
  font-size: 1rem;
  border: 0;
  &:hover {
    /* background-color: #252525; */
    border: 0;
    color: white;
    background-color: black;
    &:hover {
      background-color: #252525;
    }
  }
`;

const FoodWrapperStyles = styled.a`
  cursor: pointer;
  color: black;
  width: 14rem;
  text-decoration: none;
  margin-top: 1rem;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  text-align: center;

  img {
    height: 275px;
  }

  h3 {
    margin: 1.5rem 0;
    font-weight: 600;
  }
  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
`;

const ConfirmButtonStyles = styled.div`
  display: flex;
  justify-content: center;
  height: 100%;
  button {
    width: 100%;
    border: 0;
    color: white;
    font-size: 1.25rem;
    background-color: black;
    &:hover {
      background-color: #252525;
    }
  }
`;

export {
  HeadingStyles,
  GridContainerStyles,
  LoadMoreStyles,
  FoodWrapperStyles,
  ConfirmButtonStyles,
};
