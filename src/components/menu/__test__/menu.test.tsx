import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render, waitFor } from "@testing-library/react";
import Menu from "..";
import foods from "../../../../db/foods.json";
import foodsTest5Items from "../../../../db/foodsTest5Items.json";

describe("Menu screen component", () => {
  describe("Menu render", () => {
    test("Test heading of menu", () => {
      const { getByText } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );

      expect(getByText(/SUSHI FOOD/i)).toBeInTheDocument();
    });

    test("Display a menu had eight dishes", async () => {
      const { getByText, findAllByRole } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );
      const image = await findAllByRole("img");

      expect(getByText(/Genevieve/i)).toBeInTheDocument();
      expect(getByText(/Kemp/i)).toBeInTheDocument();
      expect(image).toHaveLength(8);
    });

    test("LOAD MORE button should not display if foods items lower than 8", async () => {
      const { getByText, findAllByRole, queryByText } = render(
        <Menu
          foods={foodsTest5Items}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );
      const image = await findAllByRole("img");

      const loadMoreButton = queryByText("LOAD MORE");
      expect(loadMoreButton).not.toBeInTheDocument();
      expect(image).toHaveLength(5);
    });
  });

  describe("Menu behavior", () => {
    test("Test LOAD MORE button to load more dishes after 1 click", async () => {
      const { getByText, findAllByRole } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );

      fireEvent.click(getByText(/LOAD MORE/i));

      const image = await findAllByRole("img");

      // Expect the first, the middle and the last dish after click LOAD MORE
      expect(getByText(/Genevieve/i)).toBeInTheDocument();
      expect(getByText(/Latasha/i)).toBeInTheDocument();
      expect(getByText(/Sheila/i)).toBeInTheDocument();

      expect(image).toHaveLength(16);
    });

    test("Test LOAD MORE button to load more dishes after 2 clicks", async () => {
      const { getByText, findAllByRole } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );

      fireEvent.click(getByText(/LOAD MORE/i));
      fireEvent.click(getByText(/LOAD MORE/i));

      const image = await findAllByRole("img");

      expect(getByText(/Genevieve/i)).toBeInTheDocument();
      expect(getByText(/Latasha/i)).toBeInTheDocument();
      expect(getByText(/Walter/i)).toBeInTheDocument();
      expect(image).toHaveLength(24);
    });

    test("Test add dish to cart", async () => {
      jest.spyOn(window.localStorage.__proto__, "setItem");
      window.localStorage.__proto__.setItem = jest.fn();

      const { getByText } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );

      fireEvent.click(getByText(/Genevieve/i));
      fireEvent.click(getByText(/Add to cart/i));

      await waitFor(() => {
        expect(localStorage.setItem).toHaveBeenCalledTimes(2);
      });
    });

    test("Test add dish to cart two times", async () => {
      jest.spyOn(window.localStorage.__proto__, "setItem");
      window.localStorage.__proto__.setItem = jest.fn();

      const { getByText } = render(
        <Menu
          foods={foods}
          setSelectedFoodQuantity={jest.fn()}
          selectedFoodQuantity={0}
        />
      );

      fireEvent.click(getByText(/Genevieve/i));
      fireEvent.click(getByText(/Add to cart/i));
      fireEvent.click(getByText(/Add to cart/i));

      await waitFor(() => {
        expect(localStorage.setItem).toHaveBeenCalledTimes(4);
      });
    });
  });
});
