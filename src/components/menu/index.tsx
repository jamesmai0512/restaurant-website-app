import { useEffect, useState } from "react";
import { ItemContentStyles } from "../Common/CommonStyledComponents/layout";
import {
  FoodWrapperStyles,
  GridContainerStyles,
  HeadingStyles,
  LoadMoreStyles,
} from "./styles";
import { SushiFood } from "../../types/SushiFood";
import Food from "../Common/Food";
import FoodDetailModal from "../Common/FoodDetailModal";

interface Props {
  foods: SushiFood[];
  setSelectedFoodQuantity: React.Dispatch<React.SetStateAction<number>>;
  selectedFoodQuantity: number;
}

function Menu({ foods, setSelectedFoodQuantity, selectedFoodQuantity }: Props) {
  const DEFAULT_PAGE_SIZE: number = 8;

  const foodData: SushiFood[] = foods;

  const [currentItems, setCurrentItems] = useState<SushiFood[]>([]);
  const [itemOffset, setItemOffset] = useState<number>(0);
  const [isCompleted, setIsCompleted] = useState<boolean>(false);

  const [selectedFood, setSelectedFood] = useState<SushiFood>({
    id: "",
    imageUrl: "",
    name: "",
    quantity: 0,
    price: 0,
  });
  const [isFoodModalDisplayed, setFoodModalDisplay] = useState<boolean>(false);

  useEffect(() => {
    setCurrentItems(foodData.slice(0, itemOffset + DEFAULT_PAGE_SIZE));

    if (itemOffset + DEFAULT_PAGE_SIZE === foodData.length) {
      setIsCompleted(true);
    }
  }, [itemOffset, setCurrentItems]);

  const handleLoadMore = () => {
    setItemOffset(itemOffset + DEFAULT_PAGE_SIZE);
  };

  const handleOpenFoodModal = (food: SushiFood) => {
    setSelectedFood(food);
    setFoodModalDisplay(true);
  };

  const handleCloseFoodModal = () => {
    setFoodModalDisplay(false);
  };

  return (
    <ItemContentStyles>
      <HeadingStyles>SUSHI FOOD</HeadingStyles>

      <GridContainerStyles>
        {currentItems.map((food) => (
          <FoodWrapperStyles
            href="#"
            key={food.id}
            onClick={() => {
              handleOpenFoodModal(food);
            }}
          >
            <Food imageUrl={food.imageUrl} name={food.name} />
          </FoodWrapperStyles>
        ))}
      </GridContainerStyles>

      {foods.length > 8 && (
        <LoadMoreStyles onClick={handleLoadMore} disabled={isCompleted}>
          LOAD MORE
        </LoadMoreStyles>
      )}

      {isFoodModalDisplayed && (
        <FoodDetailModal
          handleCloseFoodModal={handleCloseFoodModal}
          selectedFood={selectedFood}
          setSelectedFoodQuantity={setSelectedFoodQuantity}
          selectedFoodQuantity={selectedFoodQuantity}
        />
      )}
    </ItemContentStyles>
  );
}

export default Menu;
