import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render, waitFor } from "@testing-library/react";
import Cart from "..";

const mockFoodsJson = [
  {
    food: {
      id: "626e07358a8107cd0afb6b91",
      imageUrl: "https://picsum.photos/224/275",
      name: "Breakkie Roll",
      quantity: 18,
      price: 18,
    },
    quantity: 6,
    id: "1.17ng9kzyzg",
  },
];

interface Store {
  [key: string]: string;
}

const localStorageMock = (function () {
  let store: Store = {};

  return {
    getItem(key: string) {
      return store[key];
    },

    setItem(key: string, value: string) {
      store[key] = value;
    },
  };
})();

describe("Cart screen component", () => {
  describe("Cart on render", () => {
    beforeEach(() => {
      Object.defineProperty(window, "localStorage", {
        value: localStorageMock,
      });
      window.localStorage.setItem("foods", JSON.stringify(mockFoodsJson));
    });
    test("Test getItem of the cart on render", async () => {
      const { getByText, findAllByRole } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      const image = await findAllByRole("img");

      expect(image).toHaveLength(1);
      expect(getByText(/Breakkie Roll/i)).toBeInTheDocument();
      expect(localStorage.getItem("foods")).toEqual(
        JSON.stringify(mockFoodsJson)
      );
    });

    test("Test the sub total of the cart on render", async () => {
      const { getByText } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      // The subtotal equal (quantity * price)
      expect(getByText("$ 108")).toBeInTheDocument();
      expect(localStorage.getItem("foods")).toEqual(
        JSON.stringify(mockFoodsJson)
      );
    });
  });

  describe("Cart behavior", () => {
    beforeEach(() => {
      Object.defineProperty(window, "localStorage", {
        value: localStorageMock,
      });
      window.localStorage.setItem("foods", JSON.stringify(mockFoodsJson));
    });
    test("Should update sub total after decrease the quantity of the dish", () => {
      const { getByText, getByTestId } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      fireEvent.change(getByTestId("1.17ng9kzyzg"), {
        target: { value: 3 },
      });

      // The subtotal equal (quantity * price)
      expect(getByText("$ 54")).toBeInTheDocument();
    });

    test("Should update sub total after increase the quantity of the dish", () => {
      const { getByText, getByTestId } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      fireEvent.change(getByTestId("1.17ng9kzyzg"), {
        target: { value: 10 },
      });

      // The subtotal equal (quantity * price)
      expect(getByText("$ 180")).toBeInTheDocument();
    });

    test("Should remove the dish after click remove it", async () => {
      const { getByTestId, queryByText } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      fireEvent.click(getByTestId("Breakkie Roll"));
      const cartItemName = queryByText("Breakkie Roll");

      await waitFor(() => {
        expect(cartItemName).not.toBeInTheDocument();
        expect(localStorage.getItem("foods")).toEqual("[]");
      });
    });

    test("The sub total should equal to 0 $ after click remove the dish", async () => {
      const { getByText, getByTestId } = render(
        <Cart setSelectedFoodQuantity={jest.fn()} />
      );

      fireEvent.click(getByTestId("Breakkie Roll"));

      await waitFor(() => {
        expect(localStorage.getItem("foods")).toEqual("[]");
        expect(getByText("$ 0")).toBeInTheDocument();
      });
    });
  });
});
