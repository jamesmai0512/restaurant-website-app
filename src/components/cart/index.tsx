import React, { useEffect, useState } from "react";
import { ItemContentStyles } from "../Common/CommonStyledComponents/layout";
import ConfirmOrderBtn from "../Common/ConfirmOrderBtn";
import { useFoods } from "../Common/DataProvider/FoodsContext";
import FoodCart from "../Common/FoodCart";
import { HeadingStyles } from "../Menu/styles";
import {
  FlexCartItemStyles,
  FlexContainerStyles,
  FlexSubTotalStyles,
} from "./styles";

interface Props {
  setSelectedFoodQuantity: React.Dispatch<React.SetStateAction<number>>;
}

function Cart({ setSelectedFoodQuantity }: Props) {
  const { foods, setFoods } = useFoods();

  const handleCartQuantity = (
    e: React.ChangeEvent<HTMLInputElement>,
    id: string
  ) => {
    const foodIndex = foods.findIndex((i) => i.id === id);
    foods[foodIndex].quantity = Number(e.target.value);
    const newFoods = new Array(...foods);
    setFoods(newFoods);
  };

  const handleRemoveFood = (id: string) => {
    const newFoods = foods.filter((food) => food.id !== id);
    setFoods(newFoods);
  };

  const subTotalArray: number[] = [];
  const [subTotal, setSubTotal] = useState(0);

  useEffect(() => {
    let totalQuantity: number = 0;
    foods.map((food) => {
      if (food.food.price) {
        subTotalArray.push(food.food.price * food.quantity);
        totalQuantity = totalQuantity + food.quantity;
      }
    });
    setSubTotal(subTotalArray.reduce((a: number, b: number) => a + b, 0));
    setSelectedFoodQuantity(totalQuantity);
  }, [subTotal, foods]);

  return (
    <ItemContentStyles>
      <HeadingStyles>CART</HeadingStyles>

      <FlexContainerStyles>
        <FlexCartItemStyles>
          <FoodCart
            foods={foods}
            handleCartQuantity={handleCartQuantity}
            handleRemoveFood={handleRemoveFood}
          />
        </FlexCartItemStyles>
        <FlexSubTotalStyles>
          <ConfirmOrderBtn
            foods={foods}
            subTotal={subTotal}
            setFoods={setFoods}
          />
        </FlexSubTotalStyles>
      </FlexContainerStyles>
    </ItemContentStyles>
  );
}

export default Cart;
