import styled from "styled-components";

const FlexContainerStyles = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 3.125rem;
`;

const FlexCartItemStyles = styled.section`
  display: flex;
  flex-direction: column;
  flex: 7;
`;

const FlexSubTotalStyles = styled.section`
  display: flex;
  flex: 5;
`;

const CartItemWrapperStyles = styled.ul`
  background-color: #f1d5bb;
  padding: 1.25rem;
  display: flex;
  flex-direction: column;
`;

const CartItemStyles = styled.li`
  display: flex;
  align-items: center;
  padding: 1.7rem 0.5rem;
`;

const CartIconStyles = styled.img`
  width: 5.625rem;
  height: 5.625rem;
  object-fit: cover;
  border-radius: 50%;
`;

const CartDetailStyles = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0 1.2rem 0 1.2rem;
  width: 40%;
  p {
    margin: 5px;
  }
`;

const CartNameStyles = styled.p`
  font-size: 1rem;
  font-weight: 600;
`;

const CartQuantityStyles = styled.div`
  margin: auto;
  width: 10%;
  border-radius: 0;
  input {
    border: 0;
    padding: 7px 5px;
    text-align: center;
    background: white;
  }
`;

const RemoveDishStyles = styled.div`
  button {
    cursor: pointer;
    background-color: #f1d5bb;
    border: none;
    &:hover {
      color: #ccc5c5;
    }
  }
`;

const ConfirmOrderStyles = styled.div`
  width: 100%;
  min-width: 23rem;
  background-color: #f1d5bb;
  height: 17.8rem;
  padding: 1.875rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  font-family: "Mulish", sans-serif;
  font-weight: 100;
`;

const SubTotalStyles = styled.h3`
  font-weight: 100;
  font-size: 2.5rem;
  margin: 0;
`;

const TotalDetailStyles = styled.div`
  width: 70%;
  display: flex;
  justify-content: space-between;
  span {
    font-size: 30px;
  }
`;

const ConfirmButtonStyles = styled.div`
  display: flex;
  justify-content: center;
  height: 30%;
  button {
    width: 100%;
    border: 0;
    color: white;
    font-size: 1.25rem;
    background-color: black;
    &:hover {
      cursor: pointer;
      background-color: #252525;
    }
  }
`;

export {
  FlexContainerStyles,
  FlexCartItemStyles,
  FlexSubTotalStyles,
  CartItemWrapperStyles,
  CartItemStyles,
  CartIconStyles,
  CartDetailStyles,
  CartNameStyles,
  CartQuantityStyles,
  RemoveDishStyles,
  ConfirmOrderStyles,
  SubTotalStyles,
  TotalDetailStyles,
  ConfirmButtonStyles,
};
