import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { SelectedQuatityProvider } from "./components/Common/DataProvider/QuantityContext";
import App from "./app";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <BrowserRouter>
      <SelectedQuatityProvider>
        <App />
      </SelectedQuatityProvider>
    </BrowserRouter>
  </React.StrictMode>
);
